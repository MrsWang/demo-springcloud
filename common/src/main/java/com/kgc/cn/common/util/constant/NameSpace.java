package com.kgc.cn.common.util.constant;

import java.io.Serializable;

/**
 * @author wangwei
 * @date 2019/12/7 - 0:02
 * 命名空间类
 */
public class NameSpace implements Serializable {

    public static final String GOODS_NUMS_NAMESPACE = "goodsNums:";
    public static final String USER_LOGIN_NAMESPACE = "userLogin:";
    public static final String WX_LOGIN = "wxLogin:";
    public static final String LOCK_ORDERS = "lockOrders:";
    public static final String LOCK_GOODS = "lockGoods:";
    public static final String ORDER_NAMESPACE = "order:";

    public static final String REGISTERCODE = "registerCode:";
    public static final String CHECK_SPEED = "checkSpeed:";


    public static final String SHOPCAR_NAMESPACE = "shopcar:";
    public static final String MACH_BUY_DOWN = "machBuyDown:";
    public static final String MACH_BUY_END = "machBuyEnd:";
    public static final String ADVANCED_CLOSE_BUY = "advancedCloseBuy:";
    public static final String MACH_BUY_NUM = "machBuyNum:";


    public static final String SEND_CODE = "sendCode";
    public static final String GOODS_DECR_NUM = "goodsDecrNum";
    public static final String INSERT_GOODS_ORDER = "insertGoodsOrder";
    public static final String INSERT_ORDER = "insertOrder";
    public static final String UPDATE_OR_PAY_SUCCESS = "updateOrder-Goods";

    public static final String SHARE_RENT = "shareRent:";

}
