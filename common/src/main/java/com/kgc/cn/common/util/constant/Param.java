package com.kgc.cn.common.util.constant;


import java.io.Serializable;

/**
 * @author wangwei
 * @date 2019/12/7 - 0:07
 * 参数类
 */
public class Param implements Serializable {
    public static final String ORDER_VERSION = "108614";
    // 最大点击数
    public static final int MAX_CLICK = 10;
    // 设置最大点击时间
    public static final int CLICK_MOST_TIME = 5;
    // 支付超时时间
    public static final int PAY_TIMEOUT = 900;
    // 短信验证间隔时间
    public static final int CODE_TIME = 120;
    // redis加锁最大超时时间
    public static final int LOCK_TTL_TIME = 1;
    // 默认抢购前不允购物的时间
    public static final int MACH_BUY_ADVANCED_TIME = 600;

    public static final int ORDER_TIMEOUT = 900;

    public static final int LOGIN_TIMEOUT=300;


    // 正常上架状态
    public static final int GOODS_STATUS_PUTAWAY = 1;
    // 准备抢购状态
    public static final int GOODS_STATUS_PREPARE = 2;
    // 抢购状态
    public static final int GOODS_STATUS_MACHBUY = 3;


}
