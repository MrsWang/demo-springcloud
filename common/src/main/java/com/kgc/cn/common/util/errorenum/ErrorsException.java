package com.kgc.cn.common.util.errorenum;

import lombok.Data;

/**
 * @author wangwei
 * @date 2019/11/26 - 11:34
 */
@Data
public class ErrorsException extends RuntimeException {
    private String msg;
    private ErrorsMsg errorsMsg;


    public ErrorsException(String msg, Throwable throwable, ErrorsMsg errorsMsg) {
        super(msg, throwable);
        this.errorsMsg = errorsMsg;
    }

    public ErrorsException(ErrorsMsg errorsMsg) {
        super(errorsMsg.getMessge());
        this.errorsMsg = errorsMsg;
    }

    public ErrorsException(String msg) {
        super(msg);
    }

}
