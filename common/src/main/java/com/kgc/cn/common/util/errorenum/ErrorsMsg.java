package com.kgc.cn.common.util.errorenum;

/**
 * @author wangwei
 * @date 2019/11/26 - 12:06
 */
public enum ErrorsMsg {
    PHONE_OR_PWD_ERROR(101, "手机号或密码错误"),
    REGISTER_ERROR(102, "注册失败，稍后再试"),
    REGISTER_TOO(103, "用户已经注册"),
    LOGIN_OVERDUE(104, "登录过期"),
    USER_NULL(105, "该用户还未注册"),
    DATE_ERROR(106, "日期格式不正确"),
    UPDATE_USER_FAIL(108, "修改用户失败"),
    USER_IS_DEL(109, "对不起，你的账号已被锁定，无法登陆！"),
    GET_SORT_FAIL(110, "分类不存在"),
    DELETE_USER_FAIL(107, "删除用户失败"),
    SERVER_ERROR(111, "服务器繁忙，请稍后重试"),
    USERNOTFOUND(112, "未找到该用户！"),
    PHONE_ERROR(113, "电话格式不正确！"),
    MAIL_ERROR(114, "邮箱格式不正确！"),
    ROLE_ADD_FAIL(115, "员工增加失败"),


    USER_ADD_FAILD(121, "增加用户失败"),
    USER_DELETE_FAILD(122, "删除用户失败"),
    USER_ISNULL(123, "未查询到用户"),
    GOOODS_NULL(120, "未查询到商品！"),
    GOODS_ADD_FAIL(124, "增加商品失败"),
    GOODS_DELETE_FAIL(125, "商品删除失败"),
    USER_UPDATE_FAIL(126, "用户修改失败"),
    GOODS_UPDATE_FAIL(127, "商品修改失败"),
    DISCOUNT_ADD_FAIL(128, "增加折扣失败!"),
    DISCOUNT_DELETE_FAIL(129, "删除折扣失败"),
    DISCOUNT_UPDATE_FAIL(130, "更新折扣失败"),
    DISCOUNT_SELECT_FAIL(131, "查询失败"),
    DISCOUNT_ADDJURISD_FAIL(132, "权限添加失败"),
    SHOPCAR_ADD_FAIL(133, "购物车添加失败"),
    SHORTAGE_OF_GOODS(134,"商品库存不足"),
    SHOPCAR_DEL_FAIL(135, "购物车删除失败"),
    SHOPCAR_CLEAN_FAIL(136, "购物车不存在商品"),


    READ_FILE_ERROR(140, "读取文件失败"),
    USER_EXCEPTION(141,"您的账号异常"),
    CODE_FILE(142,"验证码错误"),
    GET_CODE_FILE(143,"获取验证码失败"),
    NULL_RESULT(150, "没有结果返回"),
    UPDATE_FAIL(151, "修改失败"),
    CREATE_JOB_FAIL(152, "当前正在有人添加员工"),
    ROLE_NO_GATE(153, "该员工未出勤"),
    GOODS_IS_MACHBUY(154,"该商品已经加入抢购中"),
    PHONE_IS_HAVE(155,"您已绑定手机号"),
    KUCUN_BUGOU(156,"库存不足"),
    INPUT_FAIL(157,"输入有误"),
    GOODS_READY_BUY(158,"商品即将促销，暂时购买不了"),
    CHECK_SPEED_OUT(159,"您的点击频率过快，请稍后再试"),
    BINGDING_NOT_PHONE(160,"请绑定手机"),

    RETURN_NULL(301,"没有返回数据"),
    DELETE_FAIL(303, "删除失败"),
    UPDATE_FAILD(304, "修改失败"),
    ADD_FAIL(302, "增加失败"),
    JURISDICTION_NOT(301, "权限不够");


    private ErrorsMsg(int code, String messge) {
        this.code = code;
        this.messge = messge;
    }

    private int code;
    private String messge;


    public int getCode() {
        return code;
    }


    public String getMessge() {
        return messge;
    }

}
