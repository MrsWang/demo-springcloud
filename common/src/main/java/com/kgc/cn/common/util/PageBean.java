package com.kgc.cn.common.util;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页实体类
 *
 * @author Peng
 * @Date2016年12月13日 上午9:40:10
 */
@Data
public class PageBean<T> implements Serializable {

    private int currPage;//当前页数
    private int pageSize;//每页显示的记录数
    private long totalCount;//总记录数
    private int totalPage;//总页数
    private int start;// 起始数据
    private List<T> lists;//每页的显示的数据


    /**
     * 分页整合所需数据
     *
     * @param totalCount
     * @return
     */
    public void conformity(long totalCount) {
        double promNum = Math.ceil((double) totalCount / pageSize);//向上取整
        totalPage = (int) promNum;
        //塞入总条数
        this.totalCount = totalCount;
        if (currPage > totalPage) {
            currPage = totalPage;
        }
        if (totalCount == 0) {
            currPage = 1;
        }
        start = currPage * pageSize - pageSize;
    }
}
