package com.kgc.cn.common.util;


import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTool {

    //------------------------------------------------------------------------------------------------------------------
    //                                Date ===》  String 简单类型转换
    //------------------------------------------------------------------------------------------------------------------

    /**
     * 根据 Date格式 日期取得星期几
     *
     * @param date Date格式
     * @return String 字符串
     */
    public static String getWeek(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String week = sdf.format(date);
        return week;
    }

    /**
     * Date ==》 yyyy-MM-dd HH:mm:ss
     */
    public static String formatACCURATE_SECONDS(Date date) throws Exception {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sf.format(date);
    }

    /**
     * Date ==》 yyyyMMddHHmmss；支持多并发
     */
    public static String formatYYYYMMDDHHSS(Date date) throws Exception {
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            return sf.format(date);
        } catch (Exception e) {
            System.out.println("sf.parse sysetm error;param=" + date + ";message:" + e.getMessage());
            throw new RuntimeException(e);
        }
    }


    /**
     * Date ==》  yyyy-MM-dd ，精确到天
     */
    public static String accurateDay(Date arg) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").format(arg);
    }

    /**
     * Date ==》  yyyy，精确到年
     */
    public static String accurateYear(Date arg) throws ParseException {
        return new SimpleDateFormat("yyyy").format(arg);
    }


    //------------------------------------------------------------------------------------------------------------------
    //                                 String ===》  Date 简单类型转换
    //------------------------------------------------------------------------------------------------------------------

    /**
     * yyyyMMddHHmmss ==》 Date ；支持多并发
     */
    public static Date parseYYYYMMDDHHSS(String date) throws ParseException {
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sf.parse(date);
    }

    /**
     * yyyy-MM-dd HH:mm:ss ==》 Date，精确到秒
     */
    public static Date accurateSecond(String arg) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arg);
    }

    /**
     * yyyy-MM-dd ==》  Date ，精确到天
     */
    public static Date accurateDay(String arg) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(arg);
    }

    /**
     * yyyyMMdd ==》  Date ，精确到天
     */
    public static Date accurateDayByNo(String arg) throws ParseException {
        return new SimpleDateFormat("yyyyMMdd").parse(arg);
    }


    //------------------------------------------------------------------------------------------------------------------
    //                                     对    当前系统时间   进行操作
    //------------------------------------------------------------------------------------------------------------------

    /**
     * 获得当前系统时间,精确到秒  【yyyy-MM-dd HH:mm:ss】
     */
    public static String getSysAccurateSecond() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    /**
     * 获得当前系统时间,精确到秒 【yyyyMMddHHmmss】
     */
    public static String getSysAccurateSecondYMD() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    /**
     * 获得当前系统时间,精确到天【yyyy-MM-dd】
     */
    public static String getSysAccurateDay() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /**
     * 获得当前系统时间,精确到天【yyyyMMdd】
     */
    public static String getSysAccurateDayNull() {
        return new SimpleDateFormat("yyyyMMdd").format(new Date());
    }


    /**
     * 获取当月的 天数
     */
    public static int getCurrentMonthDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 获取当前日期路径 ??
     * new Date():Thu Jul 05 15:38:41 CST 2018
     * 结果：2018\7\5
     */
    public static String getDatePath() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, 1);
        if (cal.get(Calendar.MONTH) == 0) {
            return "" + (cal.get(Calendar.YEAR) - 1) + File.separator + 12 +
                    File.separator + cal.get(Calendar.DAY_OF_MONTH);
        } else {
            return "" + cal.get(Calendar.YEAR) + File.separator + cal.get(Calendar.MONTH) +
                    File.separator + cal.get(Calendar.DAY_OF_MONTH);
        }
    }

    /**
     * 获得相对于当前系统日期的时间,精确到天【yyyyMMdd】
     * 比如：今天（2018-7-5）
     * 如果：day=2 ，那么 这个方法得到的结果：20180707 ;即在原来的时间上加 两天
     */
    public static String getSysAccurateDayNull(int day) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, day);
        return new SimpleDateFormat("yyyyMMdd").format(c.getTime());
    }


    //------------------------------------------------------------------------------------------------------------------
    //                                      对  两个时间点  进行 【判断】 【计算】
    //------------------------------------------------------------------------------------------------------------------

    /**
     * 判断两个日期是否是同一年
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameYear(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        boolean isSameYear = cal1.get(Calendar.YEAR) == cal2
                .get(Calendar.YEAR);
        return isSameYear;
    }

    /**
     * 判断两个日期是否同月
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameMonth(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        boolean isSameYear = cal1.get(Calendar.YEAR) == cal2
                .get(Calendar.YEAR);
        boolean isSameMonth = isSameYear
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);

        return isSameMonth;
    }

    /**
     * 判断两个日期是否同一天
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameDate(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        boolean isSameYear = cal1.get(Calendar.YEAR) == cal2
                .get(Calendar.YEAR);
        boolean isSameMonth = isSameYear
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
        boolean isSameDate = isSameMonth
                && cal1.get(Calendar.DAY_OF_MONTH) == cal2
                .get(Calendar.DAY_OF_MONTH);
        return isSameDate;
    }


    /**
     * 根据所给的起始,终止时间来计算间隔月数；
     */
    public static int getIntervalMonth(Date startDate, Date endDate) {
        SimpleDateFormat mmformatter = new SimpleDateFormat("MM");
        int monthstart = Integer.parseInt(mmformatter.format(startDate));
        int monthend = Integer.parseInt(mmformatter.format(endDate));
        SimpleDateFormat yyyyformatter = new SimpleDateFormat("yyyy");
        int yearstart = Integer.parseInt(yyyyformatter.format(startDate));
        int yearend = Integer.parseInt(yyyyformatter.format(endDate));
        return (yearend - yearstart) * 12 + (monthend - monthstart);
    }

    /**
     * 计算起始日期间隔多少个季度
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static int getIntervalQuarter(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        int firstM = cal.get(Calendar.MONTH);
        int firstQ = (firstM / 3) + 1;
        int firstY = cal.get(Calendar.YEAR);
        cal.setTime(endDate);
        int lastM = cal.get(Calendar.MONTH);
        int lastQ = (lastM / 3) + 1;
        int lastY = cal.get(Calendar.YEAR);
        return (lastY - firstY) * 4 + (lastQ - firstQ);
    }


    /**
     * 计算两个Date类型的时间相隔天数
     *
     * @param arg0 较小 arg0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-07-04 10:12:52");
     * @param arg1 arg1=new Date() //Thu Jul 05 14:13:40 CST 2018
     * @return 1
     */
    public static int diffDay(Date arg0, Date arg1) {
        Calendar arg2 = retainDay(arg0);
        Calendar arg3 = retainDay(arg1);
        int result = 0;
        if (arg2.getTimeInMillis() > arg3.getTimeInMillis()) {
            throw new RuntimeException("无效的参数：arg0较大");
        }

        while (arg2.compareTo(arg3) != 0) {
            arg2.add(Calendar.DAY_OF_MONTH, 1);
            result++;
        }
        return result;
    }

    /**
     * 计算两个时间相差的天数<br>
     * 包括当天
     *
     * @param date1 起期 ===> date1= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-07-04 10:12:52");
     * @param date2 止期 ===> date2=new Date() //Thu Jul 05 14:13:40 CST 2018
     * @param date1 起期 ===> date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-07-04 15:12:52");
     * @param date2 止期 ===> date2=new Date() //Thu Jul 05 14:15:30 CST 2018
     * @return 1
     */
    public static long calcDays(Date date1, Date date2) {
        return (date2.getTime() - date1.getTime()) / (1000 * 24 * 60 * 60) + 1;
    }

    /**
     * 计算两个时间相差的天数<br>
     * 仅适用于知道明确时分秒的，比如保险起止期.保险起期加1(因为00:00:00起),保险止期不加1(因为23:59:59). 推荐使用
     * com.zhongan
     * .core.creditproduct.biz.util.DateUtil.getDateDistance.加1的代码在外部程序中.
     *
     * @param date1 起期
     * @param date2 止期
     * @param day   需要加上的天数
     * @return
     */
    @Deprecated
    public static int calcDays(Date date1, Date date2, int day) {
        return (int) ((date2.getTime() - date1.getTime()) / (1000 * 24 * 60 * 60) + day);
    }


    /**
     * 计算两个时间相差的秒数<br>
     * 包括当天
     *
     * @param date1 起期
     * @param date2 止期
     * @return
     */
    public static long calcMinutes(Date date1, Date date2) {
        return (date2.getTime() - date1.getTime()) / 1000;
    }


    /**
     * @param firstDate
     * @param secondDate
     * @Description:比较两个时间点是否相等
     * @ReturnType boolean
     * @author:
     */
    public static boolean isEqual(Date firstDate, Date secondDate) {
        return compare(firstDate, secondDate) == 0 ? true : false;
    }


    /**
     * 字符串日期 判断，返回最大的日期【String-->Date;再比较】
     */
    public static Date MaxDateAccurateSecond(String arg0, String arg1) throws ParseException {
        Date date0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arg0);
        Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arg1);
        return date0.compareTo(date1) == 1 ? date0 : date1;
    }


    /**
     * @Description:比较两个时间点 如果secondDate表示的时间等于此 firstDate 表示的时间，则返回 0 值； 如果此
     * firstDate 的时间在参数<secondDate>表示的时间之前，则返回小于 0 的值； 如果此
     * firstDate 的时间在参数<secondDate>表示的时间之后，则返回大于 0 的值
     */
    public static int compare(Date firstDate, Date secondDate) {

        Calendar firstCalendar = null;
        /** 使用给定的 Date 设置此 Calendar 的时间。 **/
        if (firstDate != null) {
            firstCalendar = Calendar.getInstance();
            firstCalendar.setTime(firstDate);
        }

        Calendar secondCalendar = null;
        /** 使用给定的 Date 设置此 Calendar 的时间。 **/
        if (firstDate != null) {
            secondCalendar = Calendar.getInstance();
            secondCalendar.setTime(secondDate);
        }

        try {
            /**
             * 比较两个 Calendar 对象表示的时间值（从历元至现在的毫秒偏移量）。 如果参数表示的时间等于此 Calendar
             * 表示的时间，则返回 0 值； 如果此 Calendar 的时间在参数表示的时间之前，则返回小于 0 的值； 如果此
             * Calendar 的时间在参数表示的时间之后，则返回大于 0 的值
             **/
            return firstCalendar.compareTo(secondCalendar);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //------------------------------------------------------------------------------------------------------------------
    //                                     对 某个时间点 进行 【判断】 及 【计算】
    //------------------------------------------------------------------------------------------------------------------

    /**
     * String【yyyy-MM-dd HH:mm:ss || yyyy-MM-dd】 ==》 Date，
     * <p>
     * 模糊判断， 超过10的则精确到秒，反之精确到天
     */
    public static Date ignoreDate(String arg) throws ParseException {
        return arg.length() > 10 ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arg) : new SimpleDateFormat(
                "yyyy-MM-dd").parse(arg);
    }


    /**
     * 根据 Date 日期进行加法计算 【Date】
     * 比如：今天（Thu Jul 05 11:53:53 CST 2018）
     * 如果：day=1 ， 那么这个方法得到的结果：Fri Jul 06 11:53:53 CST 2018 ;即在原来的时间上加一天
     */
    public static Date addDate(Date date, int day) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, day);
        return c.getTime();
    }


    /**
     * 根据 Date 日期进行加法计算 【Calendar】 跟这个方法addDate 一样，只是返回类型不同
     *
     * @param date 日期
     * @param day  天数
     * @return
     */
    public static Calendar modifyDay(Date date, int day) {
        Calendar celendar = Calendar.getInstance();
        celendar.setTime(date);
        celendar.add(Calendar.DAY_OF_YEAR, day);
        return celendar;
    }


    /**
     * 获取时间，获取num天后的日期 如：num=1则返回 当前的为2014-01-01 则返回 2014-01-02 00:00:00零点的日期
     *
     * @return
     */
    public static Date getFormatDate(int num) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, num);
        String d = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(d + " 00:00:00");
    }


    /**
     * 功能：把给定日期与给定月数进行运算,月数可以是负数.返回给定日期与给定日期的差或和
     * 若形成的新日期非法,则自动对新日期进行调整,例如:2004年1月31日加1个月为2004年2月31日,系统自动调整为2004年2月29日
     */
    public static Date getDateNMonths(Date date, int months) {
        if (months == 0) { //月数为零,直接返回给定日期
            return date;
        }
        if (months > 1200 || months < -1200) { //日期限制在100年以内
            return null;
        }
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.add(Calendar.MONTH, months);
        Date date2 = gc.getTime();
        return date2;
    }


    /**
     * 把给定日期与给定季度数进行运算,返回给定日期
     *
     * @param startDate
     * @param i         给定季度数，可以为负数
     * @return
     */
    public static Date getDateNQuarters(Date startDate, int i) {
        return getDateNMonths(startDate, 3 * i);
    }


    //------------------------------------------------------------------------------------------------------------------
    //                          取得 某天 的 00:00:00 和 23:59:59
    //------------------------------------------------------------------------------------------------------------------


    /**
     * 返回【Date】格式（我也不知道这个方法有什么用？？===》 取得当天00:00:00？？ ===》没有更简单的方法吗？？？）
     * <p>
     * 比如：new Date()                                             ==>Thu Jul 05 12:03:56 CST 2018
     * DateTool.accurateDay(new Date())                       ==》2018-07-05
     * DateTool.accurateDay(DateTool.accurateDay(new Date())) ==》Thu Jul 05 00:00:00 CST 2018
     */
    public static Date getTime() throws ParseException {
        Date date = new Date();
        return DateTool.accurateDay(DateTool.accurateDay(date));
    }


    /**
     * 处理时间，只保留到天【calendar】
     * calendar.getTime(); //这就把calendar类型 转换成了 Date 类型
     * System.out.println(calendar);//java.util.GregorianCalendar[time=?,areFieldsSet=false,areAllFieldsSet=true,lenient=true,
     * zone=sun.util.calendar.ZoneInfo[id="Asia/Shanghai",offset=28800000,dstSavings=0,useDaylight=false,transitions=19,lastRule=null],
     * firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2018,MONTH=6,WEEK_OF_YEAR=27,WEEK_OF_MONTH=1,DAY_OF_MONTH=5,DAY_OF_YEAR=186,
     * DAY_OF_WEEK=5,DAY_OF_WEEK_IN_MONTH=1,AM_PM=1,HOUR=0,HOUR_OF_DAY=0,MINUTE=0,SECOND=0,MILLISECOND=0,ZONE_OFFSET=28800000,DST_OFFSET=0]
     * <p>
     * System.out.println(calendar.getTime());//Thu Jul 05 00:00:00 CST 2018
     */
    public static Calendar retainDay(Date arg) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(arg);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * 拼装时间 避免在SQL中使用函数 拼装当天开始时间 00：00：00
     *
     * @param
     * @return
     * @returnType Date
     */
    public static Date getStartDate(Date strDate) {
        if (strDate == null) {
            return null;
        }
        Calendar start = new GregorianCalendar();
        start.setTime(strDate);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        return start.getTime();

    }

    /**
     * 拼装时间 避免在SQL中使用函数 拼装当天结束时间 23：59：59
     */
    public static Date getEndDate(Date strDate) {
        if (strDate == null) {
            return null;
        }
        Calendar end = new GregorianCalendar();
        end.setTime(strDate);
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        end.set(Calendar.MILLISECOND, 0);
        return end.getTime();
    }


    /**
     * 获取该年第一天【 yyyy-MM-dd HH:mm:ss】
     * 【yyyy-01-01 00:00:00】
     */
    public static String getYearStartDate(String yearStr) throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.valueOf(yearStr), 0, 1, 0, 0, 0);
        return formatACCURATE_SECONDS(cal.getTime());
    }

    /**
     * 获取该年最后一天【 yyyy-MM-dd HH:mm:ss】
     * 【yyyy-12-31 23:59:59】
     */
    public static String getYearEndDate(String yearStr) throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.valueOf(yearStr), 11, 31, 23, 59, 59);
        return formatACCURATE_SECONDS(cal.getTime());
    }

    /**
     * 判断日期格式
     *
     * @param str
     * @return
     */
    public static boolean isValidDate(String str) {
        boolean convertSuccess = true;     //指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        //
        try {
            //设置lenient为false.否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            //如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            convertSuccess = false;
        }
        return convertSuccess;
    }

}
