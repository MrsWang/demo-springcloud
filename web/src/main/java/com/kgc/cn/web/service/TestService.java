package com.kgc.cn.web.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author wangwei
 * @date 2020/2/19 - 14:32
 */
@FeignClient(name = "service" , fallback = TestService.HystrixTest.class)
public interface TestService {
    @GetMapping(value = "test/getData")
    String getData();

    @Component
    class HystrixTest implements TestService{

        @Override
        public String getData() {
            return "错误了";
        }
    }
}
