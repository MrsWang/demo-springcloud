package com.kgc.cn.web.util.listener;

import lombok.extern.slf4j.Slf4j;
import com.kgc.cn.common.util.constant.NameSpace;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * @author wangwei
 * @date 2020/2/20 - 11:20
 **/

@Slf4j
public class KeyExpiredListener extends KeyExpirationEventMessageListener {

    public KeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String keyName = message.toString();
        log.info(keyName);
        switch (keyName) {
            case NameSpace.USER_LOGIN_NAMESPACE:
                // TODO 这里处理逻辑
                log.info(keyName);
        }
    }
}
