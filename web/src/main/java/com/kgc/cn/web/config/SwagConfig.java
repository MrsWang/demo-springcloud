package com.kgc.cn.web.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author wangwei
 * @date 2020/1/3 - 17:02
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwagConfig {
}
