package com.kgc.cn.web.controller;

import com.kgc.cn.web.service.TestService;
import com.kgc.cn.web.util.redis.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangwei
 * @date 2020/2/19 - 11:25
 */
@RestController
@RequestMapping(value = "/test")
@Api(tags = "测试类")
public class TestController {
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private TestService testService;
    @ApiOperation("测试接口")
    @GetMapping(value = "/getData")
    public String getData(){
        return testService.getData();
    }

    @ApiOperation("redis测试")
    @GetMapping(value = "/redisTest")
    public String redisTest(){
        redisUtils.set("www","qwq",5);
        return testService.getData();
    }
}
