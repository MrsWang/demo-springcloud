package com.kgc.cn.service.mapper;

/**
 * @author wangwei
 * @date 2020/2/21 - 8:47
 */
public interface TestMapper {
    String getData();
}
