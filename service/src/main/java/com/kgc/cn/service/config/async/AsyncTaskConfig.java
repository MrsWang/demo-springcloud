package com.kgc.cn.service.config.async;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * @author wangwei
 * @date 2019/12/9 - 14:18
 */
@Configuration
@EnableAsync
public class AsyncTaskConfig implements AsyncConfigurer {

    // 配置线程池
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);//最小线程数
        executor.setMaxPoolSize(10);// 最大线程数
        executor.setQueueCapacity(25);// 等待队列
        executor.initialize();//初始化
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }
}
