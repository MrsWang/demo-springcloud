package com.kgc.cn.service.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangwei
 * @date 2019/12/28 - 9:45
 */
@Configuration
@MapperScan("com.kgc.cn.service.mapper")
public class MybatisConfig {
}
