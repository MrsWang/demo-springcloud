package com.kgc.cn.service.service;

import com.kgc.cn.service.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangwei
 * @date 2020/2/19 - 15:48
 */
@RestController
@RequestMapping(value = "/test")
public class Testservice {
    @Autowired
    private TestMapper testMapper;

    @GetMapping(value = "/getData")
    public String getData(){
        return testMapper.getData();
    }
}
